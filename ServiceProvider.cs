﻿using UnityEngine;
using System.Collections.Generic;

public class ServiceProvider : MonoBehaviour {

    private Multimap<System.Type, object> container = new Multimap<System.Type, object>();


    private static ServiceProvider instance;

    private void OnEnable() {
        instance = this;
    }

    public static void Register(object value) {
        instance.container.Add(value.GetType(), value);
    }

    public static void Remove(object value) {
        instance.container.Remove(value.GetType(), value);
    }

    public static TKey GetFirst<TKey>() {
        object value;
        if (instance.container.TryGetFirst(typeof(TKey), out value)) {
            return (TKey) value;
        }
        return default(TKey);
    }
}
