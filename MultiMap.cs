﻿using System.Collections.Generic;

public class Multimap<TKey, TValue> {

    Dictionary<TKey, List<TValue>> dictionary = new Dictionary<TKey, List<TValue>>();

    public void Add(TKey key, TValue value) {
        List<TValue> valueList;
        if (!dictionary.TryGetValue(key, out valueList) ) {
            valueList = new List<TValue>();
            dictionary.Add(key, valueList);
        }
        valueList.Add(value);
    }

    public void Remove(TKey key, TValue value) {
        List<TValue> valueList;
        if (dictionary.TryGetValue(key, out valueList)) {
            valueList.Remove(value);
            if (valueList.Count == 0)
                dictionary.Remove(key);
        }
    }

    public bool TryGetFirst(TKey key, out TValue value) {
        List<TValue> valueList;
        if (dictionary.TryGetValue(key, out valueList)){
            value = valueList[0];
            return true;
        }
        value = default(TValue);
        return false;

    }
}
